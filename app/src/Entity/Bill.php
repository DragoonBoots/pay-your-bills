<?php

namespace App\Entity;

use Carbon\CarbonImmutable;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Money\Currency;
use Money\Money;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BillRepository")
 * @Vich\Uploadable()
 *
 * @Assert\Expression("this.getDate() < this.getDateDue()", message="The due date must be after the statement date.")
 */
class Bill implements OwnedEntityInterface
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Vendor
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Vendor", inversedBy="bills")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotBlank()
     */
    private $vendor;

    /**
     * @var Money
     *
     * @ORM\Column(type="money")
     *
     * @Assert\NotBlank()
     */
    private $totalAmount;

    /**
     * @var Collection|BillLine[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\BillLine", mappedBy="bill", orphanRemoval=true, cascade={"all"})
     * @ORM\OrderBy({"position": "ASC"})
     *
     * @Assert\Count(min="1", minMessage="The bill must have at least {{ limit }} line.|The bill must have at least {{ limit }} lines.")
     */
    private $lines;

    /**
     * @var CarbonImmutable
     *
     * @ORM\Column(type="carbon_immutable")
     *
     * @Assert\NotBlank()
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $dateSlug;

    /**
     * @var CarbonImmutable
     *
     * @ORM\Column(type="carbon_immutable")
     *
     * @Assert\NotBlank()
     */
    private $dateDue;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"dateSlug"}, handlers={
     *     @Gedmo\SlugHandler(class="Gedmo\Sluggable\Handler\RelativeSlugHandler", options={
     *         @Gedmo\SlugHandlerOption(name="relationField", value="vendor"),
     *         @Gedmo\SlugHandlerOption(name="relationSlugField", value="slug"),
     *         @Gedmo\SlugHandlerOption(name="separator", value="_")
     *     })
     * })
     */
    private $slug;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bills")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var EmbeddedFile
     *
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     */
    private $pdf;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="bill_pdf",
     *     fileNameProperty="pdf.name",
     *     size="pdf.size",
     *     mimeType="pdf.mimeType",
     *     originalName="pdf.originalName",
     *     dimensions="pdf.dimensions"
     * )
     */
    private $pdfFile;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(type="datetimetz_immutable", nullable=true)
     */
    private $fileDate;

    public function __construct()
    {
        $this->date = new CarbonImmutable();
        $this->dateDue = new CarbonImmutable('+15 days');
        $this->totalAmount = new Money(0, new Currency('USD'));
        $this->lines = new ArrayCollection();
        $this->pdf = new EmbeddedFile();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVendor(): ?Vendor
    {
        return $this->vendor;
    }

    public function setVendor(?Vendor $vendor): self
    {
        $this->vendor = $vendor;

        return $this;
    }

    public function addLine(BillLine $line): self
    {
        if (!$this->lines->contains($line)) {
            $this->lines[] = $line;
            $line->setBill($this);
        }

        return $this;
    }

    public function removeLine(BillLine $line): self
    {
        if ($this->lines->contains($line)) {
            $this->lines->removeElement($line);
            // set the owning side to null (unless already changed)
            if ($line->getBill() === $this) {
                $line->setBill(null);
            }
        }

        return $this;
    }

    public function getDate(): CarbonImmutable
    {
        return $this->date;
    }

    public function setDate(CarbonImmutable $date): self
    {
        $this->date = $date;
        $this->dateSlug = $date->toDateString();

        return $this;
    }

    public function getDateDue(): CarbonImmutable
    {
        return $this->dateDue;
    }

    public function setDateDue(CarbonImmutable $dateDue): self
    {
        $this->dateDue = $dateDue;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|BillLine[]
     */
    public function getLines(): Collection
    {
        return $this->lines;
    }

    /**
     * Get the total amount printed on the bill
     *
     * @return Money
     */
    public function getTotalAmount(): Money
    {
        return $this->totalAmount;
    }

    public function setTotalAmount(Money $totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function getPdfFile(): ?File
    {
        return $this->pdfFile;
    }

    public function setPdfFile(?File $pdfFile): self
    {
        $this->pdfFile = $pdfFile;
        $this->fileDate = new DateTimeImmutable();

        return $this;
    }

    public function getPdf(): EmbeddedFile
    {
        return $this->pdf;
    }

    public function setPdf(EmbeddedFile $pdf): self
    {
        $this->pdf = $pdf;

        return $this;
    }
}
