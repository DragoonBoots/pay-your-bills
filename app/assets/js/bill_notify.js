require('./app');
const $ = require('jquery');
require('datatables.net-bs4');
require('datatables-bundle/datatables');
require('../css/bill_notify.scss');
import {Calendar} from '@fullcalendar/core';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin, {Draggable} from '@fullcalendar/interaction';

$(document).ready(function () {
    const calendarContainer = document.getElementById('pyb-bill-notify-calendar-container');
    const usersListContainer = document.getElementById('pyb-bill-notify-users-list');
    const formPeriods = document.getElementById('notify_form_periods');

    // Render the calendar
    const calendar = new Calendar(calendarContainer, {
        plugins: [dayGridPlugin, interactionPlugin, bootstrapPlugin],
        droppable: true,
        themeSystem: 'bootstrap',
        timeZone: 'UTC',
        editable: true,
        eventResizableFromStart: true,
    });

    // Setup the users list
    new Draggable(usersListContainer, {itemSelector: 'li'});

    // Callbacks
    calendar.on('eventRender', function (info) {
        const element = info.el;
        const event = info.event;

        // Add delete button
        if (!info.isStart) {
            // Only add delete button to beginning of the period
            return;
        }
        const content = element.getElementsByClassName('fc-content').item(0);
        const deleteButton = document.createElement('span');
        deleteButton.classList.add('fc-event-delete');
        const deleteIcon = document.createElement('i');
        deleteIcon.classList.add('fas', 'fa-minus-circle');
        const deleteSrOnly = document.createElement('span');
        deleteSrOnly.classList.add('sr-only');
        deleteSrOnly.append('Delete');
        deleteButton.append(deleteIcon, deleteSrOnly);
        deleteButton.addEventListener('click', function () {
            event.remove();
        });

        content.append(deleteButton);
    });
    calendar.render();

    document.getElementById('pyb-bill-notify-form').addEventListener('submit', function (event) {
        const events = calendar.getEvents();
        if (events.length === 0) {
            event.preventDefault();
            alert('Add users to the calendar first.');
        }

        const periodPrototype = String(formPeriods.dataset['prototype']);
        const parser = new DOMParser();
        events.forEach(function (item, index) {
            const period = parser.parseFromString(periodPrototype.replace(/__name__/g, String(index)), 'text/html').body.firstChild;
            const userIdField = period.getElementsByClassName('pyb-bill-notify-data-user').item(0);
            const startField = period.getElementsByClassName('pyb-bill-notify-data-start').item(0);
            const endField = period.getElementsByClassName('pyb-bill-notify-data-end').item(0);

            userIdField.value = item.extendedProps['userId'];
            startField.value = item.start.toJSON();
            endField.value = item.end.toJSON();

            formPeriods.append(period);
        });
    });
});
