<?php

namespace App\Form\Type;

use App\Entity\BillLine;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BillLineType
 */
class BillLineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('split', CheckboxType::class, [
                'required' => false,
                'label' => 'Usage',
            ])
            ->add('taxRate', PercentType::class, [
                'label' => 'Tax',
                'scale' => 3,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Untaxed',
                ],
            ])->add('amount', MoneyPhpType::class, [
                'required' => false,
                'empty_data' => '0.00',
                'property_path' => 'amount',
            ])
            ->add('position', HiddenType::class, [
                'attr' => [
                    'class' => 'collection-position',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BillLine::class,
        ]);
    }
}
