<?php

namespace App\Security\Voter;

use App\Entity\OwnedEntityInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Votes on instances of OwnedEntityInterface
 *
 * Will always allow viewing, but will only allow editing if the user owns the
 * entity.
 */
class OwnedEntityVoter extends Voter
{
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, VoterAttributes::ALL, true)
            && $subject instanceof OwnedEntityInterface;
    }

    /**
     * @param string $attribute
     * @param OwnedEntityInterface $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        $owner = $subject->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // The entity has no owner set, act as if the owner is the current user.
        if (!$owner) {
            $owner = $user;
        }

        switch ($attribute) {
            case VoterAttributes::VIEW:
                return true;
            case VoterAttributes::CREATE:
            case VoterAttributes::EDIT:
            case VoterAttributes::DELETE:
                return $owner === $user;
        }

        return false;
    }
}
