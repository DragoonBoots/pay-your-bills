<?php


namespace App\Bill\Reader;

use App\Entity\Bill;

/**
 * Class BillReaderManager
 */
class BillReaderManager
{
    /**
     * @var AbstractBillReader[]
     */
    private $readers;

    /**
     * BillReaderManager constructor.
     *
     * @param AbstractBillReader[] $readers
     */
    public function __construct(iterable $readers)
    {
        $this->readers = $readers;
    }

    /**
     * @param Bill $bill
     * @return AbstractBillReader|null
     */
    public function getReaderForBill(Bill $bill)
    {
        foreach ($this->readers as $reader) {
            if ($reader->supports($bill)) {
                return $reader;
            }
        }

        return null;
    }
}
