<?php


namespace App\Bill;


use Money\Money;

final class SplitBill
{
    /**
     * @var Money
     */
    private $total;
    /**
     * @var Money
     */
    private $usageTotal;
    /**
     * @var Money
     */
    private $generalTotal;

    /**
     * @return Money
     */
    public function getTotal(): Money
    {
        return $this->total;
    }

    /**
     * @param Money $total
     * @return self
     */
    public function setTotal(Money $total): self
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return Money
     */
    public function getUsageTotal(): Money
    {
        return $this->usageTotal;
    }

    /**
     * @param Money $usageTotal
     * @return self
     */
    public function setUsageTotal(Money $usageTotal): self
    {
        $this->usageTotal = $usageTotal;
        return $this;
    }

    /**
     * @return Money
     */
    public function getGeneralTotal(): Money
    {
        return $this->generalTotal;
    }

    /**
     * @param Money $generalTotal
     * @return self
     */
    public function setGeneralTotal(Money $generalTotal): self
    {
        $this->generalTotal = $generalTotal;
        return $this;
    }
}
