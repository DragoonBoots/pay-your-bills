<?php


namespace App\Form\DataTransformer;


use Carbon\Carbon;
use Carbon\CarbonImmutable;
use DateTime;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CarbonTransformer implements DataTransformerInterface
{

    /**
     * Do nothing
     *
     * Carbon works as a DateTime, so no transform needed
     *
     * @param Carbon $value
     * @return mixed
     */
    public function transform($value)
    {
        return $value;
    }

    /**
     * Transforms a value from the transformed representation to its original
     * representation.
     *
     * This method is called when {@link Form::submit()} is called to transform the requests tainted data
     * into an acceptable format.
     *
     * The same transformers are called in the reverse order so the responsibility is to
     * return one of the types that would be expected as input of transform().
     *
     * This method must be able to deal with empty values. Usually this will
     * be an empty string, but depending on your implementation other empty
     * values are possible as well (such as NULL). The reasoning behind
     * this is that value transformers must be chainable. If the
     * reverseTransform() method of the first value transformer outputs an
     * empty string, the second value transformer must be able to process that
     * value.
     *
     * By convention, reverseTransform() should return NULL if an empty string
     * is passed.
     *
     * @param mixed $value The value in the transformed representation
     *
     * @return mixed The value in the original representation
     *
     * @throws TransformationFailedException when the transformation fails
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }
        if (!$value instanceof DateTime) {
            throw new TransformationFailedException('Expected instance of \DateTime, got ' . get_class($value));
        }

        return CarbonImmutable::createFromTimestamp($value->getTimestamp(), $value->getTimezone());
    }
}
