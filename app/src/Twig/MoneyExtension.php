<?php

namespace App\Twig;

use Money\Money;
use Money\MoneyFormatter;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class MoneyExtension
 */
class MoneyExtension extends AbstractExtension
{
    /**
     * @var MoneyFormatter
     */
    private $formatter;

    /**
     * MoneyExtension constructor.
     *
     * @param MoneyFormatter $formatter
     */
    public function __construct(MoneyFormatter $formatter)
    {
        $this->formatter = $formatter;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('money_format', [$this, 'moneyFormat']),
        ];
    }

    /**
     * @param Money $money
     * @return string
     */
    public function moneyFormat(Money $money)
    {
        return $this->formatter->format($money);
    }
}
