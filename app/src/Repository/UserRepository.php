<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param User $user
     * @return User[]
     */
    public function findOthers(User $user)
    {
        $qb = $this->createQueryBuilder('user');
        $qb->andWhere('user != :user')
            ->setParameter('user', $user);
        $q = $qb->getQuery();
        $q->execute();

        return $q->getResult();
    }

    /**
     * Find active users
     *
     * @return User[]
     */
    public function findActive()
    {
        return $this->findBy(['active' => true]);
    }
}
