<?php


namespace App\Bill\Reader;


use App\Entity\Bill;
use App\Entity\BillLine;
use ArrayIterator;
use Carbon\CarbonImmutable;
use Money\Exception\ParserException;
use Money\Money;
use Money\MoneyParser;

class RcnBillReader extends AbstractBillReader
{
    private const RE_DATE = '`\d{2}/\d{2}(?: - \d{2}/\d{2})?`';

    /**
     * @var MoneyParser
     */
    private $moneyParser;

    /**
     * RcnBillReader constructor.
     *
     * @param MoneyParser $moneyParser
     */
    public function __construct(MoneyParser $moneyParser)
    {
        $this->moneyParser = $moneyParser;
    }

    /**
     * Does this reader support reading this bill?
     *
     * @param Bill $bill
     * @return bool
     */
    public function supports(Bill $bill): bool
    {
        return $bill->getVendor()->getSlug() === 'rcn';
    }

    /**
     * Read this bill
     *
     * @param string $uploadPath
     * @param Bill $bill
     * @return Bill
     */
    public function read(string $uploadPath, Bill $bill): Bill
    {
        $text = $this->getPdfText($uploadPath, true);

        $bill->setDate($this->findDate($text))
            ->setDateDue($this->findDueDate($text))
            ->setTotalAmount($this->findTotal($text));

        $linePosition = 1;
        foreach ($this->findLines($text) as $billLine) {
            $billLine->setPosition($linePosition);
            $bill->addLine($billLine);
            $linePosition++;
        }

        return $bill;
    }

    /**
     * @param string $text
     * @return CarbonImmutable
     */
    private function findDate(string $text): CarbonImmutable
    {
        $matches = [];
        if (preg_match('`Statement Date:\s+(\d{2}/\d{2}/\d{4})`', $text, $matches)) {
            /** @var CarbonImmutable $date */
            $date = CarbonImmutable::createFromFormat('m/d/Y', $matches[1], 'UTC');
            return $date;
        }

        throw new BillReadException('Could not find bill date');
    }

    /**
     * @param string $text
     * @return CarbonImmutable
     */
    private function findDueDate(string $text): CarbonImmutable
    {
        $matches = [];
        if (preg_match('`Payment Due Date:\s+(\d{2}/\d{2}/\d{4})`', $text, $matches)) {
            /** @var CarbonImmutable $date */
            $date = CarbonImmutable::createFromFormat('m/d/Y', $matches[1], 'UTC');
            return $date;
        }

        throw new BillReadException('Could not find bill due date');
    }

    /**
     * @param string $text
     * @return Money
     */
    private function findTotal(string $text): Money
    {
        $matches = [];
        if (preg_match('`Total Amount Due:\s+(\$\d+(?:\.\d{2}))`', $text, $matches)) {
            return $this->parseAmount($matches[1]);
        }

        throw new BillReadException('Could not find bill total');
    }

    /**
     * @param string $amount
     * @return Money|null
     */
    private function parseAmount(string $amount): ?Money
    {
        try {
            $money = $this->moneyParser->parse($amount);
        } catch (ParserException $e) {
            $this->moneyParserPanic($amount, $e);

            return null;
        }

        return $money;
    }

    /**
     * @param string $text
     * @return BillLine[]
     */
    private function findLines(string $text): array
    {
        $billLines = [];
        $billIt = new ArrayIterator(explode("\n", $text));

        // Bundled services
        $this->seekToLine($billIt, 'BUNDLED SERVICES');
        $billLines = array_merge($billLines, $this->extractNonQuantityTable($billIt));
        $billIt->rewind();

        // Internet
        $this->seekToLine($billIt, 'HIGH SPEED INTERNET');
        $billLines = array_merge($billLines, $this->extractQuantityTable($billIt));
        $billIt->rewind();

        // TV
        $this->seekToLine($billIt, 'DIGITAL CABLE TV');
        $billLines = array_merge($billLines, $this->extractQuantityTable($billIt));
        $billIt->rewind();

        // Surcharges/Feeds
        $this->seekToLine($billIt, 'TAXES, SURCHARGES & FEES');
        $billLines = array_merge($billLines, $this->extractCategorizedTable($billIt, ['Internet', 'Cable']));

        return $billLines;
    }

    /**
     * @param ArrayIterator $it
     * @return BillLine[]
     */
    private function extractNonQuantityTable(ArrayIterator $it): array
    {
        $billLines = [];
        $this->seekToPattern($it, '`Date\s+Description\s+Amount`', true);
        $this->seekToNextNotNewline($it);

        $matches = [];
        while ($it->valid()
            && preg_match('`(?P<desc>.+)\s+(?P<amount>\d+\.\d{2})`', trim($it->current()), $matches)
        ) {
            $label = $matches['desc'];
            $label = preg_replace(self::RE_DATE, '', $label);
            $billLine = new BillLine();
            $billLine->setName(trim($label))
                ->setSplit(false)
                ->setTaxRate(null)
                ->setAmount($this->parseAmount('$' . $matches['amount']));
            $billLines[] = $billLine;
            $this->seekToNextNotNewline($it);
        }

        return $billLines;
    }

    /**
     * @param ArrayIterator $it
     * @return BillLine[]
     */
    private function extractQuantityTable(ArrayIterator $it): array
    {
        $billLines = [];
        $this->seekToPattern($it, '`Date\s+Description\s+Qty\s+Amount`');
        $this->seekToNextNotNewline($it);

        $matches = [];
        while ($it->valid()
            && preg_match('`(?P<desc>.+)\s+(?P<qty>\d+)\s+(?P<amount>\d+\.\d{2})`', $it->current(), $matches)
        ) {
            $label = $matches['desc'];
            $label = preg_replace(self::RE_DATE, '', $label);
            $billLine = new BillLine();
            $billLine->setName(trim($label))
                ->setSplit(false)
                ->setTaxRate(null)
                ->setAmount($this->parseAmount('$' . $matches['amount']));
            $billLines[] = $billLine;
            $this->seekToNextNotNewline($it);
        }

        return $billLines;
    }

    /**
     * @param ArrayIterator $it
     * @param array $categories
     * @return BillLine[]
     */
    private function extractCategorizedTable(ArrayIterator $it, array $categories): array
    {
        $billLines = [];
        $this->seekToPattern($it, '`Description\s+Amount`');
        $this->seekToNextNotNewline($it);

        $categoryReQuoted = [];
        foreach ($categories as $category) {
            $categoryReQuoted[] = preg_quote($category, '`');
        }
        $categoryPattern = '(?:' . implode(')|(?:', $categoryReQuoted) . ')';

        $linePattern = '(?P<desc>[^\d\n]+)\s+(?P<amount>\d+\.\d{2})';
        while ($it->valid()) {
            // If the next not-newline is a category, split where the info comes from
            $next = $this->peekNextNotNewline($it);
            $matches = [];
            if (preg_match('`' . $linePattern . '`', $next, $matches)
                && in_array(trim($matches['desc']), $categories, true)
            ) {
                // This handles when the category is on the same line as the amount, but not the description.
                $label = trim($it->current());
                $amount = $matches['amount'];
                $this->seekToNextNotNewline($it);
            } else if (preg_match('`(?P<category>' . $categoryPattern . ')\s+' . $linePattern . '`', $it->current(), $matches)) {
                // This handles when the category, description, and amount are on the same line.
                $label = trim($matches['desc']);
                $amount = $matches['amount'];
            } else if (preg_match('`' . $linePattern . '`', $it->current(), $matches)) {
                // This handles the standard case, with the description and amount on the same line and no category
                // making a mess of things.
                $label = trim($matches['desc']);
                $amount = $matches['amount'];
            } else {
                throw new BillReadException('Cannot read a categorized section.  Has the format changed?');
            }
            $billLine = new BillLine();
            $billLine->setName($label)
                ->setSplit(false)
                ->setTaxRate(null)
                ->setAmount($this->parseAmount('$' . $amount));

            // Sometimes there will be a multi-line description that needs help to work around
            $next = $this->peekNextNotNewline($it);
            $following = $this->peekNextNotNewline($it, 2);
            if (preg_match('`' . $linePattern . '`', $next)) {
                // Next line is good
                $this->seekToNextNotNewline($it);
                $break = false;
            } else if (preg_match('`' . $linePattern . '`', $following)) {
                // Multiline!!!!  Add the second line to the name and seek past it.
                $billLine->setName($billLine->getName() . ' ' . trim($next));
                $this->seekToNextNotNewline($it);
                $this->seekToNextNotNewline($it);
                $break = false;
            } else {
                $break = true;
            }

            $billLines[] = $billLine;

            if ($break) {
                break;
            }
        }

        return $billLines;
    }
}
