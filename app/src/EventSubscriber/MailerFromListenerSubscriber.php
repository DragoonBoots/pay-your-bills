<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mime\Email;

/**
 * Class MailerFromListenerSubscriber
 */
class MailerFromListenerSubscriber implements EventSubscriberInterface
{
    /**
     * @var string
     */
    private $fromAddress;

    public function __construct(string $fromAddress)
    {
        $this->fromAddress = $fromAddress;
    }

    public static function getSubscribedEvents()
    {
        return [
            MessageEvent::class => 'onMessageEvent',
        ];
    }

    /**
     * Add a global from address
     *
     * @param MessageEvent $event
     */
    public function onMessageEvent(MessageEvent $event): void
    {
        $message = $event->getMessage();

        // make sure it's an Email object
        if (!$message instanceof Email) {
            return;
        }

        // always set the from address
        $message->from($this->fromAddress);
    }
}
