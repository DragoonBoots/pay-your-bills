<?php


namespace App\Faker\Provider;


use Faker\Provider\Base as BaseProvider;

/**
 * Custom faker generators that don't fit nicely anywhere else.
 */
class CustomProvider extends BaseProvider
{
    /**
     * @param int|null $nbMaxDecimals
     * @param float $min
     * @param float|null $max
     *
     * @return float|null
     */
    public static function randomFloatOrNull(int $nbMaxDecimals = null, float $min = 0, float $max = null): ?float
    {
        return self::randomElement([null, self::randomFloat($nbMaxDecimals, $min, $max)]);
    }
}
