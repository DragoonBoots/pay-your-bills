<?php


namespace App\Doctrine\DBAL\Types;


use Carbon\Carbon;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DateTimeTzType;

/**
 * Store Carbon objects in Doctrine
 */
class CarbonType extends DateTimeTzType
{
    public function getName()
    {
        return 'carbon';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $datetime = parent::convertToPHPValue($value, $platform);

        return Carbon::instance($datetime);
    }
}
