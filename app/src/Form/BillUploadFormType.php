<?php

namespace App\Form;

use App\Entity\Bill;
use App\Entity\Vendor;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

/**
 * Class BillUploadFormType
 */
class BillUploadFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('vendor', EntityType::class, [
                'class' => Vendor::class,
                'choice_label' => 'name',
                'multiple' => false,
                'placeholder' => 'Select...',
            ])
            ->add('pdfFile', VichFileType::class, [
                'label' => 'PDF File',
                'required' => true,
                'attr' => ['placeholder' => 'Choose file...'],
            ])->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bill::class,
            'validation_groups' => false,
        ]);
    }
}
