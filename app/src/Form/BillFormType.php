<?php

namespace App\Form;

use App\Entity\Bill;
use App\Entity\BillLine;
use App\Entity\Vendor;
use App\Form\Type\BillLineType;
use App\Form\Type\CarbonDateType;
use App\Form\Type\MoneyPhpType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BillFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('vendor', EntityType::class, [
                'class' => Vendor::class,
                'choice_label' => 'name',
                'multiple' => false,
                'placeholder' => 'Select...',
            ])
            ->add('date', CarbonDateType::class, [
                'widget' => 'single_text',
            ])
            ->add('dateDue', CarbonDateType::class, [
                'widget' => 'single_text',
            ])
            ->add('totalAmount', MoneyPhpType::class)
            ->add('lines', CollectionType::class, [
                'entry_type' => BillLineType::class,
                'entry_options' => [
                    'label' => false,
                    'required' => false,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'prototype_data' => new BillLine(),
            ])
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bill::class,
        ]);
    }
}
