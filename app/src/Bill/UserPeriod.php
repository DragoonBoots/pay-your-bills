<?php


namespace App\Bill;


use App\Entity\User;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use Carbon\CarbonPeriod;

/**
 * Maps users to periods
 */
class UserPeriod
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var CarbonImmutable
     */
    private $start;

    /**
     * @var CarbonImmutable
     */
    private $end;

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param CarbonInterface $date
     * @return bool
     */
    public function contains(CarbonInterface $date): bool
    {
        return $this->getStart()->lessThanOrEqualTo($date)
            && $this->getEnd()->greaterThanOrEqualTo($date);
    }

    /**
     * @return CarbonImmutable
     */
    public function getStart(): ?CarbonImmutable
    {
        return $this->start;
    }

    /**
     * @param CarbonImmutable $start
     * @return self
     */
    public function setStart(CarbonImmutable $start): self
    {
        $this->start = $start;
        return $this;
    }

    /**
     * @return CarbonImmutable
     */
    public function getEnd(): ?CarbonImmutable
    {
        return $this->end;
    }

    /**
     * @param CarbonImmutable $end
     * @return self
     */
    public function setEnd(CarbonImmutable $end): self
    {
        $this->end = $end;
        return $this;
    }

    /**
     * @return CarbonPeriod
     */
    public function getPeriod(): CarbonPeriod
    {
        return CarbonPeriod::since($this->getStart())->days()->until($this->getEnd());
    }
}
