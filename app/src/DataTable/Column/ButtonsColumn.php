<?php


namespace App\DataTable\Column;


use Omines\DataTablesBundle\Column\AbstractColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig_Environment;

/**
 * Place buttons in a column
 */
class ButtonsColumn extends TwigColumn
{
    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * ButtonsColumn constructor.
     *
     * @param Twig_Environment $twig
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(Twig_Environment $twig, UrlGeneratorInterface $urlGenerator)
    {
        parent::__construct($twig);
        $this->twig = $twig;
        $this->urlGenerator = $urlGenerator;
    }

    protected function configureOptions(OptionsResolver $resolver)
    {
        AbstractColumn::configureOptions($resolver);

        $resolver->setDefined('buttons')
            ->setAllowedTypes('buttons', ['array'])
            ->setRequired('buttons');

        return $this;
    }

    protected function render($value, $context)
    {
        $buttons = [];
        foreach ($this->options['buttons'] as $identifier => $button) {
            $button['attr'] = array_merge($button['attr'] ?? [], [
                'class' => trim($button['attr']['class'] ?? '' . ' pyb-datatables-button-' . $identifier),
            ]);
            // Default button type
            if (!isset($button['type'])) {
                $button['type'] = 'secondary';
            }

            // Check visibility
            if (!isset($button['visible'])) {
                $button['visible'] = true;
            }
            if (is_callable($button['visible'])) {
                $button['visible'] = $button['visible']($value, $context);
            }
            if (!$button['visible']) {
                continue;
            }

            // Make sure a uri is available
            if (isset($button['uri']) || isset($button['route'])) {
                $button['element'] = 'a';
                $button['attr']['role'] = 'button';
            } else {
                $button['element'] = 'button';
                $button['attr']['type'] = 'button';
            }

            // Generate the URI if needed
            if (isset($button['route_params']) && is_callable($button['route_params'])) {
                $button['route_params'] = $button['route_params']($value, $context);
            }
            if (isset($button['route'])) {
                $button['uri'] = $this->urlGenerator->generate($button['route'], $button['route_params'] ?? []);
                unset($button['route'], $button['route_params']);
            }
            if (isset($button['uri'])) {
                $button['attr']['href'] = $button['uri'];
            }

            // Render the label
            if (!isset($button['label'])) {
                $button['label'] = '';
            }
            if (is_callable($button['label'])) {
                $button['label'] = $button($value, $context);
            }

            // Render attributes (this is helpful for data-* things)
            foreach ($button['attr'] as $attr => &$attrValue) {
                if (is_callable($attrValue)) {
                    $attrValue = $attrValue($value, $context);
                }
            }
            unset($attrValue);

            $buttons[] = $button;
        }

        return $this->twig->render('_datatable/column/buttons.html.twig', [
            'buttons' => $buttons,
            'group' => count($buttons) > 1,
        ]);
    }

}
