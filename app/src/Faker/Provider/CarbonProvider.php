<?php


namespace App\Faker\Provider;


use Carbon\CarbonImmutable;
use DateTime;
use Faker\Provider\DateTime as DateTimeProvider;

class CarbonProvider extends DateTimeProvider
{
    /**
     * @param string|DateTime $max
     * @return CarbonImmutable
     * @throws \Exception
     */
    public static function carbonDateFuture($max = '+2 months'): CarbonImmutable
    {
        $date = self::dateTimeBetween('now', $max);
        return self::convertToCarbon($date);
    }

    private static function convertToCarbon(\DateTime $dateTime): CarbonImmutable
    {
        return new CarbonImmutable('@' . $dateTime->getTimestamp());
    }

    public static function carbonDateBetween($min = '-2 months', $max = '+2 months'): CarbonImmutable
    {
        $date = self::dateTimeBetween($min, $max);
        return self::convertToCarbon($date);
    }
}
