<?php


namespace App\Doctrine\DBAL\Types;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use Money\Currency;
use Money\Money;

/**
 * Store PHPMoney objects in Doctrine
 */
class MoneyType extends IntegerType
{
    public function getName()
    {
        return 'money';
    }

    /**
     * @param Money $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        $amount = (int)$value->getAmount();

        return parent::convertToDatabaseValue($amount, $platform);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new Money($value, new Currency('USD'));
    }
}
