<?php


namespace App\Menu;


use App\Repository\BillRepository;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Knp\Menu\Iterator\RecursiveItemIterator;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class MenuBuilder
 */
class MenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;
    /**
     * @var BillRepository
     */
    private $billRepo;

    /**
     * MenuBuilder constructor.
     *
     * @param FactoryInterface $menuFactory
     * @param UserInterface|null $user
     * @param BillRepository $billRepo
     */
    public function __construct(FactoryInterface $menuFactory, ?UserInterface $user, BillRepository $billRepo)
    {
        $this->factory = $menuFactory;
        $this->billRepo = $billRepo;
        $this->user = $user;
    }

    /**
     * Main (sidebar) menu
     *
     * @param array $options
     * @return ItemInterface
     */
    public function mainMenu(array $options)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttributes(['class' => 'flex-column']);

        $recentBills = $this->billRepo->findBillsForUser($this->user, 5);
        foreach ($recentBills as $recentBill) {
            $menu->addChild(sprintf('%s (%s)',
                $recentBill->getVendor()->getName(),
                $recentBill->getDate()->toFormattedDateString()
            ), [
                'route' => 'bill_view',
                'routeParameters' => [
                    'billSlug' => $recentBill->getSlug(),
                ],
            ]);
        }

        return $this->makeBootstrapNav($menu);
    }

    /**
     * Apply the classes needed to render a bootstrap 4 nav
     *
     * @param ItemInterface $menu
     * @return ItemInterface
     */
    private function makeBootstrapNav(ItemInterface $menu): ItemInterface
    {
        // Set root attributes: Add nav class
        $menu->setChildrenAttribute('class',
            trim($menu->getChildrenAttribute('class', '') . ' nav'));

        // Set child attributes
        $itemIterator = new RecursiveItemIterator($menu);
        /** @var ItemInterface[] $iterator */
        $iterator = new \RecursiveIteratorIterator($itemIterator, \RecursiveIteratorIterator::SELF_FIRST);
        foreach ($iterator as $item) {
            $item->setAttribute('class',
                trim($item->getAttribute('class', '') . 'nav-item'));
            $item->setChildrenAttribute('class',
                trim($item->getChildrenAttribute('class', '') . ' nav'));
            $item->setLinkAttribute('class',
                trim($item->getLinkAttribute('class', '') . ' nav-link'));
        }

        return $menu;
    }
}
