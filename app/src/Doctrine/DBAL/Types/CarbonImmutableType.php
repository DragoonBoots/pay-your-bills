<?php


namespace App\Doctrine\DBAL\Types;


use Carbon\CarbonImmutable;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DateTimeTzImmutableType;
use Doctrine\DBAL\Types\JsonType;

/**
 * Store CarbonImmutable objects in Doctrine
 */
class CarbonImmutableType extends DateTimeTzImmutableType
{
    public function getName()
    {
        return 'carbon_immutable';
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $datetime = parent::convertToPHPValue($value, $platform);

        return CarbonImmutable::instance($datetime);
    }
}
