require('./app');
const $ = require('jquery');
import bsCustomFileInput from 'bs-custom-file-input';
require('../css/bill_upload.scss');

$(document).ready(function () {
    bsCustomFileInput.init();
});
