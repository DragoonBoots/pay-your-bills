<?php


namespace App\Vendor;


use App\Entity\Vendor;

/**
 * Base class for vendors
 */
abstract class AbstractVendor
{
    abstract public function supports(Vendor $vendor): bool;

}
