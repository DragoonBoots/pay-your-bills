<?php


namespace App\Bill;


use App\Entity\Bill;
use Money\Formatter\IntlMoneyFormatter;

/**
 * Validate a bill
 */
class BillValidator
{
    /**
     * @var IntlMoneyFormatter
     */
    private $moneyFormatter;
    /**
     * @var BillMather
     */
    private $billSplitter;

    /**
     * BillValidator constructor.
     *
     * @param IntlMoneyFormatter $moneyFormatter
     * @param BillMather $billSplitter
     */
    public function __construct(IntlMoneyFormatter $moneyFormatter, BillMather $billSplitter)
    {
        $this->moneyFormatter = $moneyFormatter;
        $this->billSplitter = $billSplitter;
    }

    public function getProblems(Bill $bill): array
    {
        $problems = [];

        // Check line total equals bill total, with tax applied
        $sum = $this->billSplitter->totals($bill)->getTotal();
        if (!$sum->equals($bill->getTotalAmount())) {
            $problems[] = sprintf('Bill total (%s) does not match sum of lines (%s).',
                $this->moneyFormatter->format($bill->getTotalAmount()),
                $this->moneyFormatter->format($sum)
            );
        }

        return $problems;
    }
}
