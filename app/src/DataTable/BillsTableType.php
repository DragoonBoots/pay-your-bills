<?php


namespace App\DataTable;


use App\Bill\BillValidator;
use App\DataTable\Column\ButtonsColumn;
use App\Entity\Bill;
use App\Security\Voter\VoterAttributes;
use Carbon\CarbonImmutable;
use Doctrine\ORM\QueryBuilder;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTable;
use Omines\DataTablesBundle\DataTableTypeInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class BillsTableType
 */
class BillsTableType implements DataTableTypeInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;
    /**
     * @var BillValidator
     */
    private $billValidator;
    /**
     * @var IntlMoneyFormatter
     */
    private $moneyFormatter;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * BillsTableType constructor.
     *
     * @param AuthorizationCheckerInterface $authChecker
     * @param BillValidator $billValidator
     * @param IntlMoneyFormatter $moneyFormatter
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(AuthorizationCheckerInterface $authChecker,
                                BillValidator $billValidator,
                                IntlMoneyFormatter $moneyFormatter,
                                UrlGeneratorInterface $urlGenerator)
    {
        $this->authChecker = $authChecker;
        $this->billValidator = $billValidator;
        $this->moneyFormatter = $moneyFormatter;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param DataTable $dataTable
     * @param array $options
     */
    public function configure(DataTable $dataTable, array $options)
    {
        $dataTable
            ->add('vendor', TextColumn::class, [
                'label' => 'Vendor',
                'field' => 'vendor.name',
            ])
            ->add('date', TextColumn::class, [
                'field' => 'bill.date',
                'label' => 'Date',
                'data' => static function ($context, CarbonImmutable $value) {
                    return $value->toFormattedDateString();
                },
            ])
            ->add('dateDue', TextColumn::class, [
                'field' => 'bill.dateDue',
                'label' => 'Due',
                'data' => static function ($context, CarbonImmutable $value) {
                    return $value->toFormattedDateString();
                },
            ])
            ->add('totalAmount', TextColumn::class, [
                'label' => 'Amount',
                'data' => function (Bill $context, Money $value) {
                    return $this->moneyFormatter->format($value);
                },
            ])
            ->add('valid', TextColumn::class, [
                'data' => function (Bill $context) {
                    $problems = $this->billValidator->getProblems($context);
                    if (empty($problems)) {
                        return '<span class="badge badge-success">Valid</span>';
                    }

                    return sprintf('<a href="#" class="badge badge-warning" data-toggle="tooltip" title="%s">Warning</span>',
                        implode('/n', $problems)
                    );
                },
                'label' => 'Valid',
                'raw' => true,
            ])
            ->add('actions', ButtonsColumn::class, [
                'data' => static function (Bill $context) {
                    return $context;
                },
                'label' => 'Actions',
                'buttons' => [
                    'view' => [
                        'route' => 'bill_view',
                        'route_params' => static function (Bill $bill) {
                            return ['billSlug' => $bill->getSlug()];
                        },
                        'type' => 'primary',
                        'label' => '<i class="fas fa-info-circle"></i><span class="sr-only">View</span>',
                        'visible' => function (Bill $bill) {
                            return $this->authChecker->isGranted(VoterAttributes::VIEW, $bill);
                        },
                    ],
                    'edit' => [
                        'route' => 'bill_edit',
                        'route_params' => function (Bill $bill) {
                            return [
                                'billSlug' => $bill->getSlug(),
                                'return' => $this->urlGenerator->generate('bill_index'),
                            ];
                        },
                        'label' => '<i class="fas fa-edit"></i><span class="sr-only">Edit</span>',
                        'visible' => function (Bill $bill) {
                            return $this->authChecker->isGranted(VoterAttributes::EDIT, $bill);
                        },
                    ],
                    'delete' => [
                        'type' => 'danger',
                        'attr' => [
                            'data-toggle' => 'modal',
                            'data-target' => '#pyb-bill-delete',
                            'data-bill-vendor' => static function (Bill $bill) {
                                return $bill->getVendor()->getName();
                            },
                            'data-bill-date' => static function (Bill $bill) {
                                return $bill->getDate()->toFormattedDateString();
                            },
                            'data-bill-delete-href' => function (Bill $bill) {
                                return $this->urlGenerator->generate('bill_delete', ['billSlug' => $bill->getSlug()]);
                            },
                        ],
                        'label' => '<i class="fas fa-trash"></i><span class="sr-only">Delete</span>',
                        'visible' => function (Bill $bill) {
                            return $this->authChecker->isGranted(VoterAttributes::DELETE, $bill);
                        },
                    ],
                ],
            ])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Bill::class,
                'query' => static function (QueryBuilder $qb) {
                    $qb->from(Bill::class, 'bill')
                        ->addSelect('bill')
                        ->addSelect('vendor')
                        ->join('bill.vendor', 'vendor');
                },
            ])->addOrderBy('dateDue', DataTable::SORT_ASCENDING);
    }
}
