<?php


namespace App\Security\Voter;

/**
 * Hold the attributes that can be used with voters
 */
interface VoterAttributes
{
    public const CREATE = 'create';
    public const VIEW = 'view';
    public const EDIT = 'edit';
    public const DELETE = 'delete';
    public const ALL = [
        self::CREATE, self::VIEW, self::EDIT, self::DELETE,
    ];
}
