require('./app');
const $ = require('jquery');
require('datatables.net-bs4');
require('datatables-bundle/datatables');
require('./_bill_delete');
require('../css/bill_index.scss');

$(document).ready(function () {
    // Data table
    const billTable = $('#pyb-bill-index-table');
    if (billTable.length > 0) {
        const tableSettings = billTable.data('table-settings');
        billTable.initDataTables(tableSettings);
    }
});
