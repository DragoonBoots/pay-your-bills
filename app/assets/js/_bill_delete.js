const $ = require('jquery');

$(document).ready(function () {
// Edit delete modal contents
// Store the existing data to use as a prototype
    const $deleteModal = $('#pyb-bill-delete');
    const deleteBillText = $deleteModal.find('.modal-body').text();
    $deleteModal.on('show.bs.modal', function (event) {
        const button = $(event.relatedTarget);
        const billVendor = button.data('bill-vendor');
        const billDate = button.data('bill-date');
        const deleteHref = button.data('bill-delete-href');
        const $modalBody = $(this).find('.modal-body');

        $modalBody.text(deleteBillText
            .replace('__VENDOR__', billVendor)
            .replace('__DATE__', billDate),
        );
        $(this).find('.modal-footer a').attr('href', deleteHref);
    });
});
