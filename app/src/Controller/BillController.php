<?php

namespace App\Controller;

use App\Bill\BillMather;
use App\Bill\BillValidator;
use App\Bill\Reader\BillReaderManager;
use App\Bill\Reader\BillReadException;
use App\Bill\SplitBill;
use App\DataTable\BillsTableType;
use App\Entity\Bill;
use App\Form\BillFormType;
use App\Form\BillUploadFormType;
use App\Form\NotifyFormType;
use App\Repository\UserRepository;
use App\Security\Voter\VoterAttributes;
use Money\MoneyFormatter;
use Omines\DataTablesBundle\DataTableFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BillController
 *
 * @Route(path="/bill", name="bill_")
 */
class BillController extends AbstractController
{
    private $dataTableFactory;
    /**
     * @var MoneyFormatter
     */
    private $moneyFormatter;
    /**
     * @var BillValidator
     */
    private $billValidator;
    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var BillMather
     */
    private $billMather;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * BillController constructor.
     *
     * @param DataTableFactory $dataTableFactory
     * @param MoneyFormatter $moneyFormatter
     * @param BillValidator $billValidator
     * @param BillMather $billSplitter
     * @param UserRepository $userRepository
     * @param MailerInterface $mailer
     */
    public function __construct(DataTableFactory $dataTableFactory,
                                MoneyFormatter $moneyFormatter,
                                BillValidator $billValidator,
                                BillMather $billSplitter,
                                UserRepository $userRepository,
                                MailerInterface $mailer
    )
    {
        $this->dataTableFactory = $dataTableFactory;
        $this->moneyFormatter = $moneyFormatter;
        $this->billValidator = $billValidator;
        $this->mailer = $mailer;
        $this->billMather = $billSplitter;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/", name="index")
     */
    public function index(Request $request): Response
    {
        $table = $this->dataTableFactory->createFromType(BillsTableType::class);
        $table->handleRequest($request);
        if ($table->isCallback()) {
            return $table->getResponse();
        }

        return $this->render('bill/index.html.twig', [
            'table' => $table,
        ]);
    }

    /**
     * @param Bill $bill
     * @return Response
     *
     * @Route("/view/{billSlug}", name="view")
     * @ParamConverter("bill", options={"mapping": {"billSlug": "slug"}})
     */
    public function view(Bill $bill): Response
    {
        $this->denyAccessUnlessGranted(VoterAttributes::VIEW, $bill);

        return $this->render('bill/view.html.twig', [
            'bill' => $bill,
            'lineSum' => $this->billMather->totals($bill)->getTotal(),
            'problems' => $this->billValidator->getProblems($bill),
        ]);
    }

    /**
     * @param Request $request
     * @param string $billSlug
     * @return Response
     *
     * @Route("/{billSlug}/edit", name="edit", requirements={"billSlug"=".*"})
     */
    public function edit(Request $request, string $billSlug = ''): Response
    {
        if (empty($billSlug)) {
            $bill = new Bill();
            $bill->setUser($this->getUser());
            $this->denyAccessUnlessGranted(VoterAttributes::CREATE, $bill);
        } else {
            $bill = $this->getDoctrine()->getRepository(Bill::class)->findOneBy(['slug' => $billSlug]);
            if (!$bill) {
                throw new NotFoundHttpException();
            }
            $this->denyAccessUnlessGranted(VoterAttributes::EDIT, $bill);
        }

        $form = $this->createForm(BillFormType::class, $bill);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Bill $bill */
            $bill = $form->getData();
            // Associate each line with this bill
            foreach ($bill->getLines() as $line) {
                $line->setBill($bill);
            }

            $em = $this->getDoctrine()->getManagerForClass(Bill::class);
            $em->persist($bill);
            $em->flush();
            $this->addFlash('success',
                sprintf('Updated the %s bill dated %s.',
                    $bill->getVendor()->getName(),
                    $bill->getDate()->toFormattedDateString())
            );
            return $this->redirectToPrevious($request, $this->redirectToRoute('bill_index'));
        }

        return $this->render('bill/edit.html.twig', [
            'form' => $form->createView(),
            'bill' => $bill,
        ]);
    }

    /**
     * @param Request $request
     * @param RedirectResponse $default
     * @return RedirectResponse
     */
    private function redirectToPrevious(Request $request, RedirectResponse $default): RedirectResponse
    {
        if ($request->query->has('return')) {
            return $this->redirect($request->query->get('return'));
        }

        return $default;
    }

    /**
     * @param Bill $bill
     * @return Response
     *
     * @Route("/{billSlug}/delete", name="delete")
     * @ParamConverter("bill", options={"mapping": {"billSlug": "slug"}})
     */
    public function delete(Bill $bill): Response
    {
        $this->denyAccessUnlessGranted(VoterAttributes::DELETE, $bill);
        $em = $this->getDoctrine()->getManagerForClass(Bill::class);
        $em->remove($bill);
        $em->flush();
        $this->addFlash('success', sprintf('Removed the %s bill dated %s.',
                $bill->getVendor()->getName(),
                $bill->getDate()->toFormattedDateString()
            )
        );

        return $this->redirectToRoute('bill_index');
    }

    /**
     * @param Request $request
     * @param Bill $bill
     * @return Response
     *
     * @Route("/{billSlug}/split", name="split")
     * @ParamConverter("bill", options={"mapping": {"billSlug": "slug"}})
     */
    public function split(Request $request, Bill $bill): Response
    {
        $this->denyAccessUnlessGranted(VoterAttributes::VIEW, $bill);
        $users = $this->userRepository->findActive();

        $form = $this->createForm(NotifyFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $result = $form->getData()['periods'];
            $split = $this->billMather->split($bill, $result, $users);
            return $this->render('bill/split.results.html.twig', [
                'split' => $split,
                'totals' => $this->billMather->totals($bill),
                'bill' => $bill,
            ]);
//            $mails = $this->buildEmails($bill, $this->billMather->totals($bill), $split);
//            $sent = [];
//            $failed = [];
//            foreach ($mails as $mail) {
//                try {
//                    $this->mailer->send($mail);
//                    $sent[] = $mail->getTo()[0];
//                } catch (TransportExceptionInterface $e) {
//                    $failed[] = $mail->getTo()[0];
//                }
//                if ($sent) {
//                    $this->addFlash('success', 'Message sent.');
//                }
//                if ($failed) {
//                    $this->addFlash('error', 'An error occurred sending message.');
//                }
//            }
//
//            return $this->redirectToPrevious($request, $this->redirectToRoute('bill_index'));
        }

        return $this->render('bill/split.html.twig', [
            'bill' => $bill,
            'users' => $users,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param BillReaderManager $billReaderManager
     * @return Response
     *
     * @Route(path="/upload", name="upload")
     */
    public function upload(Request $request, BillReaderManager $billReaderManager): Response
    {
        $bill = new Bill();
        $this->denyAccessUnlessGranted(VoterAttributes::CREATE, $bill);

        $form = $this->createForm(BillUploadFormType::class, $bill);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Bill $bill */
            $bill = $form->getData();
            $reader = $billReaderManager->getReaderForBill($bill);
            try {
                $bill = $reader->read($bill->getPdfFile()->getPathname(), $bill);
                $bill->setUser($this->getUser());

                $em = $this->getDoctrine()->getManagerForClass(Bill::class);
                $em->persist($bill);
                $em->flush();
                $em->refresh($bill);
                return $this->redirectToRoute('bill_edit', ['billSlug' => $bill->getSlug()]);
            } catch (BillReadException $e) {
                $this->addFlash('error', sprintf('An error occured reading the uploaded file: %s', $e->getMessage()));
            }
        }

        return $this->render('bill/upload.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Bill $bill
     * @param SplitBill $totals
     * @param array $split
     * @return Email[]
     */
    private function buildEmails(Bill $bill, SplitBill $totals, array $split): array
    {
        $mails = [];

        foreach ($split as $splitInfo) {
            $user = $splitInfo['user'];
            $amount = $splitInfo['total'];

            // Build the email
            $mail = new TemplatedEmail();
            $mail->replyTo($bill->getUser()->getEmail())
                ->to($user->getEmail())
                ->subject(sprintf('Please pay %s %s for %s by %s',
                    $bill->getUser()->getEmail(),
                    $this->moneyFormatter->format($amount),
                    $bill->getVendor()->getName(),
                    $bill->getDateDue()->toFormattedDateString()
                ))
                ->htmlTemplate('bill/notify.mail.html.twig')
                ->context([
                    'bill' => $bill,
                    'user' => $user,
                    'amount' => $amount,
                    'totals' => $totals,
                ]);
            $mails[] = $mail;
        }

        return $mails;
    }
}
