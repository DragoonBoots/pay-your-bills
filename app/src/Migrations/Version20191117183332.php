<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Initial DB
 */
final class Version20191117183332 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initial DB setup';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE bill_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE vendor_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bill_line_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bill (id INT NOT NULL, vendor_id INT NOT NULL, user_id INT NOT NULL, total_amount INT NOT NULL, date TIMESTAMP(0) WITH TIME ZONE NOT NULL, date_slug VARCHAR(255) NOT NULL, date_due TIMESTAMP(0) WITH TIME ZONE NOT NULL, slug VARCHAR(255) NOT NULL, file_date TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, pdf_name VARCHAR(255) DEFAULT NULL, pdf_original_name VARCHAR(255) DEFAULT NULL, pdf_mime_type VARCHAR(255) DEFAULT NULL, pdf_size INT DEFAULT NULL, pdf_dimensions TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7A2119E3989D9B62 ON bill (slug)');
        $this->addSql('CREATE INDEX IDX_7A2119E3F603EE73 ON bill (vendor_id)');
        $this->addSql('CREATE INDEX IDX_7A2119E3A76ED395 ON bill (user_id)');
        $this->addSql('COMMENT ON COLUMN bill.total_amount IS \'(DC2Type:money)\'');
        $this->addSql('COMMENT ON COLUMN bill.date IS \'(DC2Type:carbon_immutable)\'');
        $this->addSql('COMMENT ON COLUMN bill.date_due IS \'(DC2Type:carbon_immutable)\'');
        $this->addSql('COMMENT ON COLUMN bill.file_date IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN bill.pdf_dimensions IS \'(DC2Type:simple_array)\'');
        $this->addSql('CREATE TABLE vendor (id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F52233F6989D9B62 ON vendor (slug)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, change_password BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE TABLE bill_line (id INT NOT NULL, bill_id INT NOT NULL, name TEXT NOT NULL, description TEXT DEFAULT NULL, split BOOLEAN NOT NULL, tax_rate VARCHAR(255) DEFAULT NULL, amount INT NOT NULL, position INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_220BDC5C1A8C12F5 ON bill_line (bill_id)');
        $this->addSql('COMMENT ON COLUMN bill_line.amount IS \'(DC2Type:money)\'');
        $this->addSql('ALTER TABLE bill ADD CONSTRAINT FK_7A2119E3F603EE73 FOREIGN KEY (vendor_id) REFERENCES vendor (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bill ADD CONSTRAINT FK_7A2119E3A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bill_line ADD CONSTRAINT FK_220BDC5C1A8C12F5 FOREIGN KEY (bill_id) REFERENCES bill (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE bill_line DROP CONSTRAINT FK_220BDC5C1A8C12F5');
        $this->addSql('ALTER TABLE bill DROP CONSTRAINT FK_7A2119E3F603EE73');
        $this->addSql('ALTER TABLE bill DROP CONSTRAINT FK_7A2119E3A76ED395');
        $this->addSql('DROP SEQUENCE bill_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE vendor_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE bill_line_id_seq CASCADE');
        $this->addSql('DROP TABLE bill');
        $this->addSql('DROP TABLE vendor');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE bill_line');
    }
}
