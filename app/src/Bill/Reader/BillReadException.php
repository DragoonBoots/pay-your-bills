<?php


namespace App\Bill\Reader;


use RuntimeException;

/**
 * Thrown when an error occurs while reading a bill PDF.
 */
class BillReadException extends RuntimeException
{

}
