<?php

namespace App\Form\Type;

use App\Form\DataTransformer\CarbonTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Carbon date-only form type
 */
class CarbonDateType extends AbstractType
{
    /**
     * @var CarbonTransformer
     */
    private $carbonTransformer;

    public function __construct(CarbonTransformer $carbonTransformer)
    {
        $this->carbonTransformer = $carbonTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->addModelTransformer($this->carbonTransformer);
    }


    public function getParent()
    {
        return DateType::class;
    }

}
