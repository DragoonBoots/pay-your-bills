<?php


namespace App\Bill\Reader;


use App\Entity\Bill;
use App\Entity\BillLine;
use ArrayIterator;
use Carbon\CarbonImmutable;
use Money\Exception\ParserException;
use Money\Money;
use Money\MoneyParser;

/**
 * Class ConEdisonBillReader
 */
class ConEdisonBillReader extends AbstractBillReader
{
    private const TAXRATE = '0.045';
    /**
     * @var MoneyParser
     */
    private $moneyParser;

    /**
     * ConEdisonBillReader constructor.
     *
     * @param MoneyParser $moneyParser
     */
    public function __construct(MoneyParser $moneyParser)
    {
        $this->moneyParser = $moneyParser;
    }

    /**
     * Does this reader support reading this bill?
     *
     * @param Bill $bill
     * @return bool
     */
    public function supports(Bill $bill): bool
    {
        return $bill->getVendor()->getSlug() === 'conedison';
    }

    /**
     * Read this bill
     *
     * @param string $uploadPath
     * @param Bill $bill
     * @return Bill
     */
    public function read(string $uploadPath, Bill $bill): Bill
    {
        $text = $this->getPdfText($uploadPath);

        $bill->setDate($this->findDate($text))
            ->setDateDue($this->findDueDate($text))
            ->setTotalAmount($this->findTotal($text));

        $linePosition = 1;
        foreach ($this->findLines($text) as $billLine) {
            $billLine->setPosition($linePosition);
            $bill->addLine($billLine);
            $linePosition++;
        }

        return $bill;
    }

    /**
     * @param string $text
     * @return CarbonImmutable
     */
    private function findDate(string $text): CarbonImmutable
    {
        $matches = [];
        if (preg_match(
            '`Your billing summary as of ((?:(?:Jan)|(?:Feb)|(?:Mar)|(?:Apr)|(?:May)|(?:Jun)|(?:Jul)|(?:Aug)|(?:Sep)|(?:Oct)|(?:Nov)|(?:Dec)) \d{1,2}, \d{4})`',
            $text,
            $matches)
        ) {
            /** @var CarbonImmutable $date */
            $date = CarbonImmutable::createFromFormat('M j, Y', $matches[1], 'UTC');
            return $date;
        }

        throw new BillReadException('Could not find bill date');
    }

    /**
     * @param string $text
     * @return CarbonImmutable
     */
    private function findDueDate(string $text): CarbonImmutable
    {
        $matches = [];
        if (preg_match('`Pay By (\d{2}/\d{2}/\d{2})`', $text, $matches)) {
            /** @var CarbonImmutable $date */
            $date = CarbonImmutable::createFromFormat('m/d/y', $matches[1], 'UTC');
            return $date;
        }

        throw new BillReadException('Could not read date due');
    }

    /**
     * @param string $text
     * @return Money
     */
    private function findTotal(string $text): Money
    {
        $matches = [];
        if (preg_match('`Total new charges\n\n(\$\d+(?:\.\d{2}))`', $text, $matches)) {
            return $this->parseAmount($matches[1]);
        }

        throw new BillReadException('Could not find bill total');
    }

    /**
     * @param string $amount
     * @return Money|null
     */
    private function parseAmount(string $amount): ?Money
    {
        try {
            $money = $this->moneyParser->parse($amount);
        } catch (ParserException $e) {
            $this->moneyParserPanic($amount, $e);

            return null;
        }

        return $money;
    }

    /**
     * @param string $text
     * @return BillLine[]
     */
    private function findLines(string $text): array
    {
        $billLines = [];
        $billIt = new ArrayIterator(explode("\n", $text));

        // LX Delivery charges
        $this->seekToLine($billIt, 'Your electricity charges');
        $this->seekToSectionHeader($billIt, 'Your delivery charges');
        $billLines = array_merge($billLines, $this->extractLines($billIt));
        $billIt->rewind();

        // Gas Delivery charges
        $this->seekToLine($billIt, 'Your gas charges');
        $this->seekToSectionHeader($billIt, 'Your delivery charges');
        $billLines = array_merge($billLines, $this->extractLines($billIt));
        $billIt->rewind();

        // LX/Gas Supply charges
        // This will be in a bizarre back-and-forth format, e.g.
        //
        // Customer charge
        // Supply cost @14.9159¢ per Wh
        // Sales tax @4.5000%
        //
        // $0.00
        // $65.63
        // $2.95
        $this->seekToLine($billIt, 'Your electricity supply detail');
        $this->seekToLine($billIt, 'Your electricity supply charges');
        if ($this->peekNextNotNewline($billIt) !== 'Your gas supply charges') {
            // Failed the sanity check
            $this->supplySectionPanic();
        }
        $this->seekToLine($billIt, 'Your gas supply charges');
        $this->seekToNextNotNewline($billIt);
        $billLines = array_merge($billLines, $this->extractSupplySectionTable($billIt));
        $this->seekToNextNotNewline($billIt);
        $billLines = array_merge($billLines, $this->extractSupplySectionTable($billIt));

        return $billLines;
    }

    /**
     * @param ArrayIterator $it
     * @param string $sectionLabel
     */
    private function seekToSectionHeader(ArrayIterator $it, string $sectionLabel): void
    {
        $this->seekToLine($it, $sectionLabel);

        // Iterator is now sitting on section label, go to the next line and return
        $it->next();
    }

    /**
     * @param ArrayIterator $it
     * @return array
     */
    private function extractLines(ArrayIterator $it): array
    {
        $billLines = [];

        while ($it->valid()) {
            /** @var string $lineLabel */
            $lineLabel = trim($it->current());
            $next = trim($this->peekNextNotNewline($it));
            // Don't want these lines separately
            if (stripos($lineLabel, 'total') !== false
                || stripos($lineLabel, 'sales tax') !== false) {
                // The first not-new-line will be the amount
                $this->seekToNextNotNewline($it);
                $this->seekToNextNotNewline($it);
                continue;
            }
            if (!$this->isBillLine($it)) {
                break;
            }

            $billLine = new BillLine();
            $billLine->setTaxRate(self::TAXRATE)
                ->setName($lineLabel)
                ->setAmount($this->parseAmount($next))
                ->setSplit($this->isSplit($lineLabel));

            if ($this->lineIsCredit($lineLabel)) {
                $billLine->setAmount($billLine->getAmount()->negative());
            }

            $billLines[] = $billLine;
            // The first not-new-line will be the amount
            $this->seekToNextNotNewline($it);
            $this->seekToNextNotNewline($it);
        }

        return $billLines;
    }

    /**
     * @param ArrayIterator $it
     * @return bool
     */
    private function isBillLine(ArrayIterator $it): bool
    {
        if (preg_match('`^\$\d+(?:\.\d{2})$`', trim($this->peekNextNotNewline($it)))) {
            return true;
        }

        return false;
    }

    /**
     * @param string $lineLabel
     * @return bool
     */
    private function isSplit(string $lineLabel): bool
    {
        return strpos($lineLabel, '@') !== false;
    }

    /**
     * @param string $lineLabel
     * @return bool
     */
    private function lineIsCredit(string $lineLabel): bool
    {
        return stripos($lineLabel, 'credit') !== false;
    }

    /**
     *
     */
    private function supplySectionPanic(): void
    {
        throw new BillReadException('Cannot parse supply section of the bill.  Has the format changed?');
    }

    /**
     * @param ArrayIterator $it
     * @return BillLine[]
     */
    private function extractSupplySectionTable(ArrayIterator $it): array
    {
        // First get the labels
        $rows = [];
        $billLines = [];
        do {
            if (!$it->valid()) {
                $this->supplySectionPanic();
            }

            $line = $it->current();
            if (stripos($line, 'sales tax') !== false) {
                // A null will tell the next process to skip this row.
                $rows[] = null;
            } else {
                $rows[] = $line;
            }
            if ($this->isBillLine($it)) {
                $it->next();
                break;
            }

            $it->next();
        } while ($it->valid());
        $this->seekToNextNotNewline($it);

        // Now get the amounts
        foreach ($rows as $lineLabel) {
            if ($lineLabel === null) {
                continue;
            }
            $amount = $it->current();
            $billLine = new BillLine();
            $billLine->setTaxRate(self::TAXRATE)
                ->setName($lineLabel)
                ->setAmount($this->parseAmount($amount))
                ->setSplit($this->isSplit($lineLabel));
            if ($this->lineIsCredit($lineLabel)) {
                $billLine->setAmount($billLine->getAmount()->negative());
            }

            $billLines[] = $billLine;
            $it->next();
            if (!$it->valid()) {
                $this->supplySectionPanic();
            }
        }

        return $billLines;
    }
}
