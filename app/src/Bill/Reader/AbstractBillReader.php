<?php


namespace App\Bill\Reader;


use App\Entity\Bill;
use ArrayIterator;

/**
 * Class AbstractBillReader
 */
abstract class AbstractBillReader
{
    /**
     * Does this reader support reading this bill?
     *
     * @param Bill $bill
     * @return bool
     */
    abstract public function supports(Bill $bill): bool;

    /**
     * Read this bill
     *
     * @param string $uploadPath
     * @param Bill $bill
     * @return Bill
     */
    abstract public function read(string $uploadPath, Bill $bill): Bill;

    /**
     * @param string $pdfPath
     * @param bool $keepLayout
     * @return string
     */
    protected function getPdfText(string $pdfPath, bool $keepLayout = false): string
    {
        $args = [
            escapeshellarg($pdfPath),
            '-',
        ];

        if ($keepLayout) {
            array_unshift($args, '-layout');
        }

        $output = [];
        $ret = 0;
        exec('pdftotext ' . implode(' ', $args), $output, $ret);
        if ($ret > 0) {
            throw new BillReadException('Error reading uploaded PDF.');
        }

        return implode("\n", $output);
    }

    /**
     * @param ArrayIterator $it
     * @param int $offset
     * @return string
     */
    protected function peekNextNotNewline(ArrayIterator $it, int $offset = 1): string
    {
        $pos = $it->key();

        for ($i = 0; $i < $offset; $i++) {
            $this->seekToNextNotNewline($it);
        }
        $next = $it->current();

        // Restore position
        $it->seek($pos);

        return $next;
    }

    /**
     * @param ArrayIterator $it
     */
    protected function seekToNextNotNewline(ArrayIterator $it): void
    {
        $it->next();
        while ($it->valid() && empty($it->current())) {
            $it->next();
        }
    }

    /**
     * @param ArrayIterator $it
     * @param string $line
     */
    protected function seekToLine(ArrayIterator $it, string $line): void
    {
        while ($it->valid()) {
            if (trim($it->current()) === $line) {
                return;
            }
            $it->next();
        }
    }

    /**
     * @param string $amount
     * @param \Exception|null $e
     */
    protected function moneyParserPanic(string $amount, \Exception $e = null): void
    {
        throw new BillReadException(sprintf('Error parsing amount "%s"', $amount), $e->getCode(), $e);
    }

    /**
     * @param ArrayIterator $it
     * @param string $pattern
     */
    protected function seekToPattern(ArrayIterator $it, string $pattern, bool $trim = false): void
    {
        while ($it->valid()) {
            $check = $it->current();
            if ($trim) {
                $check = trim($check);
            }
            if (preg_match($pattern, $check)) {
                return;
            }
            $it->next();
        }
    }
}
