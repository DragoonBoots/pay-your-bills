<?php


namespace App\Bill\Reader;


use App\Entity\Bill;
use App\Entity\BillLine;
use ArrayIterator;
use Carbon\CarbonImmutable;
use Money\Exception\ParserException;
use Money\Money;
use Money\MoneyParser;

class SpectrumBillReader extends AbstractBillReader
{
    /**
     * @var MoneyParser
     */
    private $moneyParser;

    /**
     * SpectrumBillReader constructor.
     *
     * @param MoneyParser $moneyParser
     */
    public function __construct(MoneyParser $moneyParser)
    {
        $this->moneyParser = $moneyParser;
    }

    /**
     * Does this reader support reading this bill?
     *
     * @param Bill $bill
     * @return bool
     */
    public function supports(Bill $bill): bool
    {
        return $bill->getVendor()->getSlug() === 'spectrum';
    }

    /**
     * Read this bill
     *
     * @param string $uploadPath
     * @param Bill $bill
     * @return Bill
     */
    public function read(string $uploadPath, Bill $bill): Bill
    {
        $text = $this->getPdfText($uploadPath, false);

        $bill->setDate($this->findDate($text))
            ->setDateDue($this->findDueDate($text))
            ->setTotalAmount($this->findTotal($text));

        $linePosition = 1;
        foreach ($this->findLines($text) as $billLine) {
            $billLine->setPosition($linePosition);
            $bill->addLine($billLine);
            $linePosition++;
        }

        return $bill;
    }

    /**
     * @param string $text
     * @return CarbonImmutable
     */
    private function findDate(string $text): CarbonImmutable
    {
        $matches = [];
        if (preg_match('`^((?:(?:January)|(?:February)|(?:March)|(?:April)|(?:May)|(?:June)|(?:July)|(?:August)|(?:September)|(?:October)|(?:November)|(?:December)) \d{1,2}, \d{4})`', $text, $matches)) {
            /** @var CarbonImmutable $date */
            $date = CarbonImmutable::createFromFormat('F j, Y', $matches[1], 'UTC');
            return $date;
        }

        throw new BillReadException('Could not find bill date');
    }

    /**
     * @param string $text
     * @return CarbonImmutable
     */
    private function findDueDate(string $text): CarbonImmutable
    {
        $matches = [];
        if (preg_match('`Total Due by (\d{2}/\d{2}/\d{2})`', $text, $matches)) {
            /** @var CarbonImmutable $date */
            $date = CarbonImmutable::createFromFormat('m/d/y', $matches[1], 'UTC');
            return $date;
        }

        throw new BillReadException('Could not find bill due date');
    }

    /**
     * @param string $text
     * @return Money
     */
    private function findTotal(string $text): Money
    {
        $billIt = new ArrayIterator(explode("\n", $text));

        $this->seekToLine($billIt, 'Summary');
        $this->seekToPattern($billIt, '`Service from .+`');
        $this->seekToNextNotNewline($billIt);
        $this->seekToNextNotNewline($billIt);

        $billLines = $this->extractLines($billIt);
        $billIt->rewind();

        foreach ($billLines as $billLine) {
            if (strpos($billLine->getName(), 'Total Due by') === 0) {
                return $billLine->getAmount();
            }
        }

        throw new BillReadException('Could not find bill total');
    }

    /**
     * @param string $amount
     * @return Money|null
     */
    private function parseAmount(string $amount): ?Money
    {
        if (!preg_match('`(?P<negative>-)?(?P<amount>\d+\.\d{2})`', $amount, $amountParts)) {
            $this->moneyParserPanic($amount);
        }

        try {
            $money = $this->moneyParser->parse('$' . $amountParts['amount']);
        } catch (ParserException $e) {
            $this->moneyParserPanic($amount, $e);

            return null;
        }
        if (!empty($amountParts['negative'])) {
            $money = $money->negative();
        }

        return $money;
    }

    /**
     * @param string $text
     * @return BillLine[]
     */
    private function findLines(string $text): array
    {
        $billLines = [];
        $billIt = new ArrayIterator(explode("\n", $text));
        $this->seekToLine($billIt, 'Charge Details');

        // Internet
        $this->seekToLine($billIt, 'Internet Services');
        $this->seekToNextNotNewline($billIt);
        $billLines = array_merge($billLines, $this->extractLines($billIt));
        $billIt->rewind();
        $this->seekToLine($billIt, 'Charge Details');

        return $billLines;
    }

    /**
     * @param ArrayIterator $it
     * @return BillLine[]
     */
    private function extractLines(ArrayIterator $it): array
    {
        // First get the labels
        $rows = [];
        $billLines = [];
        do {
            if (!$it->valid()) {
                $this->billLinePanic();
            }

            $line = $it->current();
            if (stripos($line, 'sales tax') !== false) {
                // A null will tell the next process to skip this row.
                $rows[] = null;
            } else {
                $rows[] = $line;
            }
            if (trim($line) === '' || $this->isBillLine($it)) {
                $it->next();
                break;
            }

            $it->next();
        } while ($it->valid());
        $this->seekToNextNotNewline($it);

        // Now get the amounts
        foreach ($rows as $lineLabel) {
            if ($lineLabel === null) {
                continue;
            }
            $amount = $it->current();
            $billLine = new BillLine();
            $billLine->setTaxRate(null)
                ->setName($lineLabel)
                ->setAmount($this->parseAmount($amount))
                ->setSplit(true);

            $billLines[] = $billLine;
            $it->next();
            if (!$it->valid()) {
                $this->billLinePanic();
            }
        }

        return $billLines;
    }

    /**
     * @param ArrayIterator $it
     * @return bool
     */
    private function isBillLine(ArrayIterator $it): bool
    {
        if (preg_match('`^\$?-?\d+(?:\.\d{2})$`', trim($this->peekNextNotNewline($it)))) {
            return true;
        }

        return false;
    }

    private function billLinePanic()
    {
        throw new BillReadException('Cannot parse the bill.  Has the format changed?');
    }
}
