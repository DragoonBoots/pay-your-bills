<?php

namespace App\Repository;

use App\Entity\Bill;
use App\Security\Voter\VoterAttributes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Bill|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bill|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bill[]    findAll()
 * @method Bill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BillRepository extends ServiceEntityRepository
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    /**
     * BillRepository constructor.
     *
     * @param RegistryInterface $registry
     * @param AuthorizationCheckerInterface $authChecker
     */
    public function __construct(RegistryInterface $registry, AuthorizationCheckerInterface $authChecker)
    {
        parent::__construct($registry, Bill::class);
        $this->authChecker = $authChecker;
    }

    /**
     * @param UserInterface $user
     * @param int $limit
     * @return Bill[]
     */
    public function findBillsForUser(?UserInterface $user, int $limit)
    {
        $bills = [];

        $qb = $this->createQueryBuilder('bill');
        $qb->addOrderBy('bill.date', 'ASC')
            ->setFirstResult(0)
            ->setMaxResults(100);
        $paginator = new Paginator($qb->getQuery(), false);
        foreach ($paginator as $bill) {
            if ($this->authChecker->isGranted(VoterAttributes::VIEW, $bill)) {
                $bills[] = $bill;
            }
            if (count($bills) >= $limit) {
                break;
            }
        }

        return $bills;
    }
}
