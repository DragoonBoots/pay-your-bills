<?php


namespace App\Faker\Provider;


use Faker\Provider\Base as BaseProvider;
use Money\Currency;
use Money\Money;

/**
 * Generates random amounts of money
 */
class MoneyProvider extends BaseProvider
{
    public static function money(int $min = 0, int $max = 20000)
    {
        return new Money(random_int($min, $max), new Currency('USD'));
    }
}
