<?php


namespace App\Entity;

/**
 * Entity with ownership
 */
interface OwnedEntityInterface
{
    /**
     * @return User
     */
    public function getUser(): ?User;
}
