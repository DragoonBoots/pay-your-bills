<?php


namespace App\Form\ViewTransformer;


use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money;
use Money\MoneyFormatter;
use Money\MoneyParser;
use Money\Parser\DecimalMoneyParser;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class MoneyPhpViewTransformer
 */
class MoneyPhpViewTransformer implements DataTransformerInterface
{
    /**
     * @var string
     */
    private $currency;

    /**
     * MoneyPhpViewTransformer constructor.
     *
     * @param string $currency
     */
    public function __construct(string $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @param Money|string|null $value
     * @return string
     */
    public function transform($value)
    {
        if ($value === null) {
            $value = new Money(0, new Currency($this->currency));
        }
        if (!$value instanceof Money) {
            try {
                $value = $this->parser()->parse($value, new Currency($this->currency));
            } catch (\InvalidArgumentException $e) {
                throw new TransformationFailedException('Value is not a valid money format', 0, $e);
            }
        }

        return $this->formatter()->format($value);
    }

    /**
     * @return MoneyParser
     */
    private function parser(): MoneyParser
    {
        static $parser = null;
        if (!$parser) {
            $currencies = new ISOCurrencies();
            $parser = new DecimalMoneyParser($currencies);
        }

        return $parser;
    }

    private function formatter(): MoneyFormatter
    {
        static $formatter = null;
        if (!$formatter) {
            $currencies = new ISOCurrencies();
            $formatter = new DecimalMoneyFormatter($currencies);
        }

        return $formatter;
    }

    /**
     * @param string $value
     * @return string
     */
    public function reverseTransform($value)
    {
        if (empty($value)) {
            $value = '0.00';
        }

        return $value;
    }
}
