require('./app');
const $ = require('jquery');
require('./_stretchy');
require('symfony-collection');
require('./_bill_delete');
require('../css/bill_edit.scss');

$(document).ready(function () {
    // Enable bill line editing
    $('.pyb-bill-edit-lines').collection({
        elements_selector: 'tr.collection-item',
        elements_parent_selector: '%id% tbody',
        position_field_selector: '.collection-position',
        init_with_n_elements: 1,
        add: '<button type="button" class="btn btn-success collection-add"><i class="fas fa-plus-circle"></i><span class="sr-only">Add line</span></button>',
    });
});
