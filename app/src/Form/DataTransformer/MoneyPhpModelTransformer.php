<?php


namespace App\Form\DataTransformer;


use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Money;
use Money\MoneyParser;
use Money\Parser\DecimalMoneyParser;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class MoneyPhpTransformer
 */
class MoneyPhpModelTransformer implements DataTransformerInterface
{
    /**
     * @var string
     */
    private $currency;

    /**
     * MoneyPhpModelTransformer constructor.
     *
     * @param string $currency
     */
    public function __construct(string $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @param Money|string|null $value
     * @return Money
     */
    public function transform($value)
    {
        if ($value === null) {
            return new Money(0, new Currency($this->currency));
        }
        if (!$value instanceof Money) {
            try {
                $value = new Money($value, new Currency($this->currency));
            } catch (\InvalidArgumentException $e) {
                throw new TransformationFailedException('Value is not a valid money format', 0, $e);
            }
        }

        return $value;
    }

    /**
     * @param mixed $value
     * @return mixed|Money
     */
    public function reverseTransform($value)
    {
        try {
            $money = $this->parser()->parse($value, new Currency($this->currency));
        } catch (\InvalidArgumentException $e) {
            throw new TransformationFailedException('Value is not a valid money format', 0, $e);
        }

        return $money;
    }

    /**
     * @return MoneyParser
     */
    private function parser(): MoneyParser
    {
        static $parser = null;
        if (!$parser) {
            $currencies = new ISOCurrencies();
            $parser = new DecimalMoneyParser($currencies);
        }

        return $parser;
    }
}
