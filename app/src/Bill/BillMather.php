<?php


namespace App\Bill;


use App\Entity\Bill;
use App\Entity\BillLine;
use App\Entity\User;
use Carbon\CarbonImmutable;
use Carbon\CarbonPeriod;
use Money\Currency;
use Money\Money;
use Money\MoneyFormatter;

/**
 * Class BillMather
 */
class BillMather
{
    /**
     * @var MoneyFormatter
     */
    private $moneyFormatter;

    /**
     * BillSplitter constructor.
     *
     * @param MoneyFormatter $moneyFormatter
     */
    public function __construct(MoneyFormatter $moneyFormatter)
    {
        $this->moneyFormatter = $moneyFormatter;
    }

    /**
     * @param Bill $bill
     * @param UserPeriod[] $userPeriods
     * @param User[] $users
     *   A list of active users
     * @return array[]
     *   A list of arrays.  Each sub array has the keys "user" and "amount".
     */
    public function split(Bill $bill, array $userPeriods, array $users): array
    {
        $periodRange = $this->periodRange($userPeriods);
        $periodLength = $periodRange->count();
        $totals = $this->totals($bill);
        $usagePart = $totals->getUsageTotal()->divide($periodLength);

        // Zeroth pass: How many users apply?
        $split = [];
        foreach ($userPeriods as $userPeriod) {
            $user = $userPeriod->getUser();
            $split[$user->getId()] = [];
        }

        // First pass: create an list containing the day and how many pieces that day is split into.
        $days = [];
        foreach ($periodRange as $day) {
            $dayId = $day->getTimestamp();
            $days[$dayId] = [
                'day' => $day,
                'parts' => 0,
            ];
            foreach ($userPeriods as $userPeriod) {
                if ($userPeriod->contains($day)) {
                    $days[$dayId]['parts']++;
                }
            }
        }

        // Second pass: divide the amount into chunks for each day, then divide those chunks into parts for
        // each user present on that day.  The end result of this is that presence on a given day costs a
        // certain amount.
        foreach ($days as &$dayInfo) {
            if ($dayInfo['parts'] < 1) {
                // No users were set for this period, so assume all active users
                $dayInfo['parts'] = count($users);
            }
            $usageChunk = $usagePart->divide($dayInfo['parts']);
            // Everyone pays this part
            $dayInfo['amount'] = $usageChunk;
        }
        unset($dayInfo);

        // Third pass: total each user's contribution.
        $generalChunk = $totals->getGeneralTotal()->divide(count($users));
        foreach ($users as $user) {
            $userId = $user->getId();
            $split[$userId] = [
                'user' => $user,
                'general' => $generalChunk,
                'usage' => new Money(0, new Currency('USD')),
            ];
            foreach ($userPeriods as $userPeriod) {
                if ($userPeriod->getUser() !== $user) {
                    continue;
                }
                foreach ($days as $dayInfo) {
                    if (!$userPeriod->contains($dayInfo['day'])) {
                        continue;
                    }
                    $split[$userId]['usage'] = $split[$userId]['usage']->add($dayInfo['amount']);
                }
            }
            $split[$userId]['total'] = $split[$userId]['general']->add($split[$userId]['usage']);
        }

        return $split;
    }

    /**
     * @param UserPeriod[] $userPeriods
     * @return CarbonPeriod
     */
    private function periodRange(array $userPeriods): CarbonPeriod
    {
        // The end of time
        $start = CarbonImmutable::maxValue();
        // The beginning of time
        $end = CarbonImmutable::minValue();
        foreach ($userPeriods as $userPeriod) {
            $start = $start->min($userPeriod->getStart());
            $end = $end->max($userPeriod->getEnd());
        }

        return CarbonPeriod::since($start)->days()->until($end);
    }

    /**
     * @param Bill $bill
     * @return SplitBill
     */
    public function totals(Bill $bill): SplitBill
    {
        [$usageLines, $generalLines] = $this->sortLinesBySplit($bill->getLines());
        // Sanity check to ensure there's something to work with for the rest of the process
        if (empty($usageLines)) {
            $usageLines[] = $this->dummyLine(true);
        }
        if (empty($generalLines)) {
            $generalLines[] = $this->dummyLine(false);
        }

        $lines = $this->applyTax($bill->getLines());
        $usageLines = $this->applyTax($usageLines);
        $generalLines = $this->applyTax($generalLines);
        $allAmounts = $this->getAmounts($lines);
        $usageAmounts = $this->getAmounts($usageLines);
        $generalAmounts = $this->getAmounts($generalLines);
        $total = Money::sum(...$allAmounts);
        $usageTotal = Money::sum(...$usageAmounts);
        $generalTotal = Money::sum(...$generalAmounts);

        $response = new SplitBill();
        $response->setTotal($total)
            ->setGeneralTotal($generalTotal)
            ->setUsageTotal($usageTotal);

        return $response;
    }

    /**
     * @param BillLine[] $lines
     * @return array
     *   BillLine[] $splitLines, BillLine[] $noSplitLines
     */
    private function sortLinesBySplit(iterable $lines): iterable
    {
        $splitLines = [];
        $noSplitLines = [];
        foreach ($lines as $line) {
            if ($line->getSplit()) {
                $splitLines[] = $line;
            } else {
                $noSplitLines[] = $line;
            }
        }

        return [$splitLines, $noSplitLines];
    }

    private function dummyLine(bool $split): BillLine
    {
        $line = new BillLine();
        $line->setName('None')
            ->setAmount(new Money(0, new Currency('USD')))
            ->setPosition(-1)
            ->setSplit($split)
            ->setTaxRate('0');

        return $line;
    }

    /**
     * @param BillLine[] $lines
     * @return BillLine[]
     */
    private function applyTax(iterable $lines): iterable
    {
        $taxedLines = [];
        foreach ($lines as $line) {
            $line = clone $line;
            $line->setAmount($line->getAmount()->multiply($line->getTaxRate() + 1));
            $taxedLines[] = $line;
        }

        return $taxedLines;
    }

    /**
     * @param BillLine[] $lines
     * @return Money[]
     */
    private function getAmounts(iterable $lines): iterable
    {
        $amounts = [];
        foreach ($lines as $line) {
            $amounts[] = $line->getAmount();
        }

        return $amounts;
    }
}
