<?php

namespace App\Form\Type;

use App\Form\DataTransformer\MoneyPhpModelTransformer;
use App\Form\ViewTransformer\MoneyPhpViewTransformer;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Money form type
 */
class MoneyPhpType extends MoneyType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('currency', 'USD')
            ->setDefault('divisor', 100);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new MoneyPhpModelTransformer($options['currency']))
            ->addViewTransformer(new MoneyPhpViewTransformer($options['currency']));
    }
}
