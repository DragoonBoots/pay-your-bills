<?php

namespace App\Form\Type;

use App\Bill\UserPeriod;
use App\Entity\User;
use App\Repository\UserRepository;
use Carbon\CarbonImmutable;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class NotifyFormPeriodType
 */
class NotifyFormPeriodType extends AbstractType
{
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * NotifyFormPeriodType constructor.
     *
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', HiddenType::class, [
                'attr' => ['class' => 'pyb-bill-notify-data-user'],
            ])
            ->add('start', HiddenType::class, [
                'attr' => ['class' => 'pyb-bill-notify-data-start'],
            ])
            ->add('end', HiddenType::class, [
                'attr' => ['class' => 'pyb-bill-notify-data-end'],
            ]);

        $builder->get('user')->addModelTransformer(new CallbackTransformer(static function (?User $user) {
            return $user ? $user->getId() : null;
        }, function (int $userId) {
            return $this->userRepo->findOneBy(['id' => $userId]);
        }));

        $builder->get('start')->addModelTransformer(new CallbackTransformer(static function (?CarbonImmutable $carbon) {
            return json_encode($carbon);
        }, static function (string $json) {
            return CarbonImmutable::parse($json, 'UTC');
        }));

        // We don't care about times, and adjusting the entity by one microsecond keeps the end date inclusive UTC.
        $builder->get('end')->addModelTransformer(new CallbackTransformer(static function (?CarbonImmutable $carbon) {
            if (!$carbon) {
                return json_encode(null);
            }
            $adjusted = $carbon->addMicrosecond();
            return json_encode($adjusted);
        }, static function (string $json) {
            $carbon = CarbonImmutable::parse($json, 'UTC');
            return $carbon->subMicrosecond();
        }));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserPeriod::class,
        ]);
    }
}
